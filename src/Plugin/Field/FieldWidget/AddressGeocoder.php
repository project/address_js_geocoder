<?php

namespace Drupal\address_js_geocoder\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\address\Plugin\Field\FieldWidget\AddressDefaultWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\geocoder\FormatterPluginManager;
use Drupal\geocoder_field\GeocoderFieldPluginManager;
use Drupal\geocoder\GeocoderInterface;
use Drupal\address_js_geocoder\Ajax\GeocodeAddressCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Plugin implementation of the 'address_geocoder' widget.
 *
 * @FieldWidget(
 *   id = "address_geocoder",
 *   module = "address_js_geocoder",
 *   label = @Translation("Address geocoder"),
 *   field_types = {
 *     "address"
 *   }
 * )
 */
class AddressGeocoder extends AddressDefaultWidget implements ContainerFactoryPluginInterface {

  /**
   * The country repository.
   *
   * @var \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  protected $countryRepository;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The plugin manager for this type of plugin.
   *
   * @var \Drupal\geocoder_field\GeocoderFieldPluginManager
   */
  protected $geocoderFieldPluginManager;

  /**
   * The field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The geocoder formatter plugin manager.
   *
   * @var \Drupal\geocoder\FormatterPluginManager
   */
  protected $geocoderFormatterPluginManager;

  /**
   * The geocoder formatter plugin.
   *
   * @var \Drupal\geocoder\Plugin\Geocoder\Formatter\FormatterInterface
   */
  protected $geocoderFormatter;

  /**
   * The geocoder service.
   *
   * @var \Drupal\geocoder\GeocoderInterface
   */
  protected $geocoder;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a AddressDefaultWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \CommerceGuys\Addressing\Country\CountryRepositoryInterface $country_repository
   *   The country repository.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\geocoder\FormatterPluginManager $geocoder_formatter_plugin_manager
   *   The geocoder formatter plugin manager service.
   * @param \Drupal\geocoder_field\GeocoderFieldPluginManager $geocoder_field_plugin_manager
   *   The plugin manager for this type of plugin.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   The discoverer of entity fields.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\geocoder\GeocoderInterface $geocoder
   *   The gecoder service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, CountryRepositoryInterface $country_repository, EventDispatcherInterface $event_dispatcher, ConfigFactoryInterface $config_factory, FormatterPluginManager $geocoder_formatter_plugin_manager, GeocoderFieldPluginManager $geocoder_field_plugin_manager, EntityFieldManager $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, GeocoderInterface $geocoder) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $country_repository, $event_dispatcher, $config_factory);

    $this->geocoderFormatterPluginManager = $geocoder_formatter_plugin_manager;
    $this->geocoderFormatter = $this->geocoderFormatterPluginManager->createInstance('default_formatted_address');
    $this->geocoderFieldPluginManager = $geocoder_field_plugin_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->geocoder = $geocoder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // @see \Drupal\Core\Field\WidgetPluginManager::createInstance().
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('address.country_repository'),
      $container->get('event_dispatcher'),
      $container->get('config.factory'),
      $container->get('plugin.manager.geocoder.formatter'),
      $container->get('geocoder_field.plugin.manager.field'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('geocoder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'geofield_target' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [] + parent::settingsForm($form, $form_state);

    $elements['geofield_target'] = [
      '#type' => 'textfield',
      '#title' => t('Geofield target'),
      '#default_value' => $this->getSetting('geofield_target'),
      '#description' => t('The geofield field machine name that change this address will update.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [] + parent::settingsSummary();

    if (!empty($this->getSetting('geofield_target'))) {
      $summary[] = t('Changing this address will update: @geofield', ['@geofield' => $this->getSetting('geofield_target')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $field_name = $this->fieldDefinition->getName();

    $element['#attached']['drupalSettings']['address_selector'] = 'edit-' . str_replace('_', "-", $field_name) . "-" . $delta;
    $element['#attached']['library'][] = 'address_js_geocoder/address_js_geocoder-library';

    $element['request_geocode'] = [
      '#type' => 'button',
      '#value' => t('Get resolved addresses'),
      '#name' => "request_geocode",
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [$this, 'requestGeocodeCallback'],
    // This element is updated with this AJAX callback.
        'wrapper' => 'geocoded-select-wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Requesting geocode...'),
        ],
      ],
    ];

    $element['geocoded_select'] = [
      '#type' => 'select',
      '#title' => $this->t('Resolved addresses options'),
      '#description' => $this->t("This option will be positioned at map below"),
      '#empty_value' => '',
      '#empty_option' => '- Select a value -',
      '#default_value' => '',
      '#prefix' => '<div id="geocoded-select-wrapper">',
      '#suffix' => '</div>',
      '#validated' => TRUE,
      '#options' => [],
      '#ajax' => [
        'callback' => [$this, 'geocodedSelectedCallback'],
      // 'disable-refocus' => FALSE,
      // Or TRUE to prevent re-focusing on the triggering element.
      // 'event' => 'change',.
      // This element is updated with this AJAX callback.
        'wrapper' => 'edit-output',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Showing in map...'),
        ],
      ],
    ];

    // Create a textbox that will be updated
    // when the user selects an item from the select box above.
    $element['container']['resolved_address'] = [
      '#type' => 'hidden',
      '#size' => '60',
      '#disabled' => TRUE,
      '#value' => '',
    ];

    $element['autogeocode'] = [
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Automatically position on map'),
      '#default_value' => TRUE,
    ];

    return $element;
  }

  /**
   * Use when is implemented the autosubmit of address via fapi not js.
   *
   * Adds ajax callback to all address fields.
   *
   * @param array $element
   *   The render array element to modify.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param array $form
   *   The form.
   *
   * @return array
   *   The form element.
   */
  public function addressMapTriggering(array $element, FormStateInterface &$form_state, array $form) {
    // @todo WHENEVER it is implemented the autosubmit of address via FAPI not
    // jquery;.
    $trigger_fields = ['address_line1', /*'address_line2',*/ 'postal_code', 'locality', 'country'];
    foreach ($trigger_fields as $trigger_field) {
      $element[$trigger_field]['#ajax'] = [
        'callback' => ['Drupal\address_js_geocoder\Plugin\Field\FieldWidget\AddressGeocoder', 'autocheckCallback'],
      ];
    }
    return $element;
  }

  /**
   * Use when is implemented the autosubmit of address via fapi not js.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function autocheckCallback(array &$form, FormStateInterface $form_state) {
    // @todo when is implemented the autosubmit of address via fapi
    // It must fill the hidden field after that wait a timeout and
    // submit value to be geocoded... But address is multifield and so
    // on it's not easy to bind an event ti this callback.
    $field_name = $this->fieldDefinition->getName();
    $response = new AjaxResponse();
    $address = $form_state->getValue($field_name);

    $address_string = $this->addressToString($address);
    $form[$field_name]["widget"][0]['container']["resolved_address"]["value"] = $address_string;
    // @todo BELOW pass to data drupal selector  $form[$field_name]['#attributes']['data-drupal-selector'];.
    $response->addCommand(new ReplaceCommand("#resolved_address-output", $form[$field_name]["widget"][0]['resolved_address']));
    return $response;

  }

  /**
   * From geocoded_select field fill geofield target.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function geocodedSelectedCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $field_name = $this->fieldDefinition->getName();
    $triggering_element = $form_state->getTriggeringElement();

    if ($selectedValue = NestedArray::getValue($form_state->getValues(), $triggering_element['#parents'])) {
      $coords_array = explode("|", $selectedValue);
      $coords = [
        "latitude" => $coords_array[0],
        "longitude" => $coords_array[1],
      ];

      $address_widget_selector_path = array_merge($triggering_element['#array_parents'], [
        '#attributes',
        'data-drupal-selector',
      ]);
      $address_widget_selector = NestedArray::getValue($form, $address_widget_selector_path);

      $closer_parent_entity = $this->lookupCloserParentPath($triggering_element['#array_parents']);
      $target_geofield_selector_path = array_merge($closer_parent_entity, [
        $this->getSetting('geofield_target'),
        '#attributes',
        'data-drupal-selector',
      ]);
      $target_geofield_selector = NestedArray::getValue($form, $target_geofield_selector_path);
      // @todo hem de passar field_name de field_adreca[0][value][lon] a particular_profiles[0][entity][field_adreca][0][value][lat].
      $closer_parent = $this->lookupCloserParentPath($triggering_element['#parents']);
      $address_field_name = $this->buildElementName($closer_parent, $this->field_name);
      $target_field_name = $this->buildElementName($closer_parent, $this->getSetting('geofield_target'));
      $response->addCommand(
            new GeocodeAddressCommand(
                [
                  "widget" => $address_widget_selector,
                  "field_name" => $address_field_name,
                ], $coords, [
                  "widget" => $target_geofield_selector,
                  "field_name" => $target_field_name,
                ],
            )
        );
    }
    return $response;

  }

  /**
   * Gets the path to closer entity stack where the element is.
   *
   * It works with array_parents and parents from triggering_element.
   *
   * @param array $parents
   *   Array_parents and parents from triggering_element.
   *
   * @return array
   *   An array with the path in form of array to closer parent.
   */
  public function lookupCloserParentPath($parents) {
    for ($i = count($parents) - 1; $i >= 0; $i--) {
      if ($parents[$i] != 'entity') {
        array_pop($parents);
      }
      else {
        return $parents;
      }
    }
    return $parents;
  }

  /**
   * Builds the element name that is used to get to element in js.
   *
   * @param array $parents
   *   The parents of the element.
   * @param string $field_machine_name
   *   The field machine name of the element.
   *
   * @return string
   *   A string with the name to a form element in DOM.
   */
  public function buildElementName($parents, $field_machine_name) {
    if (count($parents)) {
      // As it comes from path from array build the name from array.
      $top_name = array_shift($parents);
      $top_name .= '[' . implode('][', $parents) . ']';
      $top_name .= "[$field_machine_name]";
      return $top_name;
    }
    else {
      // As no parents array path use just the field_machine_name.
      return $field_machine_name;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $triggering_element = $form_state->getTriggeringElement();
    // @todo Check if geofield_target exists.
    $entity = $form_state->getFormObject()->getEntity();
    // Check if the top entity in array_parents of this element has the
    // address_field or we need to go deeper
    // (address field is in a referenced entity)
    $form_display = $form_state->getStorage()['form_display'];
    // @todo add a way to obtain field definitions of the most closer parent
    // as with the next logic cannot go further than
    // parent[field_nested] -> entity -> field_address.
    $top_parent_entity_form_field_definitions = $form_display->get('fieldDefinitions')[$triggering_element['#array_parents'][0]];
    $top_parent_entity_form_settings = $top_parent_entity_form_field_definitions->getSettings();

    if ($top_parent_entity_form_field_definitions->getType() == 'entity_reference') {
      // @todo check for the containing type to not hardcode the bundle as the
      // profile_type below.
      // @todo ADDTESTS check between multiple types of parent entities
      // e.g. profile, media, group, etc
      // @todo allow more nesting here:
      // for example user->profile->media->field_adress
      $top_parent_entity_fields = $this->entityFieldManager->getFieldDefinitions($top_parent_entity_form_settings['target_type'], array_keys($top_parent_entity_form_settings['handler_settings']['target_bundles'])[0]);
      $target_field_definition = $top_parent_entity_fields[$this->getSetting('geofield_target')];
    }
    elseif ($top_parent_entity_form_field_definitions->getType() == 'address') {
      // No nested entity, so we can use the fields of the entity belonging to
      // the form directly:
      $target_field = $form_state->getFormObject()->getEntity()->getFields()[$this->getSetting('geofield_target')];
      if ($target_field) {
        $target_field_definition = $target_field->getFieldDefinition();
      }
      else {
        $target_field_definition = FALSE;
      }
    }

    if (!$target_field_definition) {
      // The array path to point to address field inputs elements.
      $form_state->setErrorByName($triggering_element['#parents'], $this->t("The target @field field doesn't exist.", [
        '@field' => $this->getSetting('geofield_target'),
      ]));
      // The array path to point to select element.
      $geocoded_select_array_path = array_slice($triggering_element['#array_parents'], 0, -1);
      $geocoded_select_array_path[] = 'geocoded_select';
      $geocoded_select = NestedArray::getValue($form, $geocoded_select_array_path);
      NestedArray::setValue(array_merge($geocoded_select_array_path, ['#options']), []);
      return $geocoded_select;
    }
  }

  /**
   * An Ajax callback to request the geocode via geocode api.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function requestGeocodeCallback(array &$form, FormStateInterface $form_state) {
    // Check if geofield_target exists.
    $entity = $form_state->getFormObject()->getEntity();
    if ($form_state->getErrors()) {
      // If there are errors, we can show the form again with the errors in
      // the status_messages section.
      $response = new AjaxResponse();
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new OpenModalDialogCommand($this->t('Errors'), $form, ['width' => '50%']));
      return $response;
    }

    $resolve_button = $form_state->getTriggeringElement();

    // The array path to point to address field inputs elements
    // $address_input_array_path[] = 'address';.
    $address_input_array_path = array_slice($resolve_button['#parents'], 0, -2);

    // The array path to point to select element.
    $geocoded_select_array_path = array_slice($resolve_button['#array_parents'], 0, -1);
    $geocoded_select_array_path[] = 'geocoded_select';
    $geocoded_select = NestedArray::getValue($form, $geocoded_select_array_path);

    // Parents and array parents are distinct:
    // #array_parents contains the actual parent array keys of an element
    // in a form structure.
    // #parents always contains the parent array keys of an element in the
    // submitted form values.
    $parents = $resolve_button['#array_parents'];

    // Check if the top entity in array_parents of this element has the
    // address_field or we need to go deeper
    // (address field is in a referenced entity)
    $form_display = $form_state->getStorage()['form_display'];
    // @todo add a way to obtain field definitions of the most closer parent as
    // with the next logic cannot go further than
    // parent[field_nested] -> entity -> field_address.
    $top_parent_entity_form_field_definitions = $form_display->get('fieldDefinitions')[$resolve_button['#array_parents'][0]];
    if (empty($this->getSetting('geofield_target'))) {
      $response = new AjaxResponse();
      $message = $this->t("The geofield target is not set. Contact administrator.");
      $response->addCommand(new OpenModalDialogCommand($this->t('Errors'), $message, [
        'width' => '50%',
        'minHeight' => 200,
        'resizable' => TRUE,
      ]));
      return $response;
    }

    $top_parent_entity_form_settings = $top_parent_entity_form_field_definitions->getSettings();

    if ($top_parent_entity_form_field_definitions->getType() == 'entity_reference') {
      // @todo check for the containing type to not hardcode the bundle as the profile_type below
      // @todo ADDTESTS check between multiple types of parent entities:
      // e.g. profile, media, group, etc
      // @todo allow more nesting here (for example user->profile->media->field_adress)
      $top_parent_entity_fields = $this->entityFieldManager->getFieldDefinitions($top_parent_entity_form_settings['target_type'], array_keys($top_parent_entity_form_settings['handler_settings']['target_bundles'])[0]);
      $target_field_definition = $top_parent_entity_fields[$this->getSetting('geofield_target')];
    }
    elseif ($top_parent_entity_form_field_definitions->getType() == 'address') {
      // No nested entity, so we can use the fields of the entity belonging
      // to the form directly:
      $target_field_definition = $form_state->getFormObject()->getEntity()->getFields()[$this->getSetting('geofield_target')]->getFieldDefinition();
    }
    if (!$target_field_definition) {
      $response = new AjaxResponse();
      $message = $this->t("The target '@field' field doesn't exist. Contact administrator.", [
        '@field' => $this->getSetting('geofield_target'),
      ]);
      $response->addCommand(new OpenModalDialogCommand($this->t('Errors'), $message, [
        'width' => '50%',
        'minHeight' => 200,
        'resizable' => TRUE,
      ]));
      return $response;
    }

    // Check if target field has geocode enabled.
    /** @var \Drupal\geocoder_field\GeocoderFieldPluginInterface $field_plugin */
    if (!$target_field_plugin = $this->geocoderFieldPluginManager->getPluginByFieldType($target_field_definition->getType())) {
      // There's no geocoding field plugin to handle this type of field.
      $response = new AjaxResponse();
      $message = $this->t("There's no geocoding field plugin in '@field' to handle this type of field. Contact administrator.", ['@field' => $this->getSetting('geofield_target')]);
      $response->addCommand(new OpenModalDialogCommand($this->t('Errors'), $message, [
        'width' => '50%',
        'minHeight' => 200,
        'resizable' => TRUE,
      ]));
      return $response;
    }
    $target_field_config = $target_field_definition->getThirdPartySettings('geocoder_field');
    $providers_ids = $target_field_definition->getThirdPartySettings('geocoder_field')["providers"];
    $providers = $this->entityTypeManager->getStorage('geocoder_provider')->loadMultiple($providers_ids);

    // Prepare our textfield checking if the select field has selected option.
    if ($address = NestedArray::getValue($form_state->getUserInput(), $address_input_array_path)) {

      $address_string = $this->addressToString($address);
      // Geocode the address:
      $addressCollection = $this->geocoder->geocode($address_string, $providers);

      // Set select options from results!!!
      $geocode_opts = $addressCollection;

      // Reset the options.
      $geocoded_select['#options'] = [];

      // Fill the select options.
      foreach ($geocode_opts as $key => $val) {
        $formatted_place = $this->geocoderFormatter->format($val);
        // $val->getFormattedAddress();
        $geocoded_select['#options'][$val->getCoordinates()->getLatitude() . "|" . $val->getCoordinates()->getLongitude()] = $formatted_place;
      }
    }

    $response = new AjaxResponse();
    // Return commands Replace the options from search
    // and trigger select change event to populate the map.
    $response->addCommand(new ReplaceCommand("#geocoded-select-wrapper", $geocoded_select));
    $response->addCommand(new InvokeCommand('#geocoded-select-wrapper select', 'trigger', ['change']));

    return $response;
  }

  /**
   * Sets the address from Adress element type to string.
   *
   * @param array $address_fields
   *   The address fields from address->value.
   */
  protected function addressToString(array $address_fields) {

    $addresses = [];
    foreach ($address_fields as $delta => $item) {
      $address[] = !empty($item["address"]['address_line1']) ? $item["address"]['address_line1'] : NULL;
      $address[] = !empty($item["address"]['address_line2']) ? $item["address"]['address_line2'] : NULL;
      $address[] = !empty($item["address"]['postal_code']) ? $item["address"]['postal_code'] : NULL;
      $address[] = !empty($item["address"]['locality']) ? $item["address"]['locality'] : NULL;
      $address[] = !empty($item["address"]['country']) ? $item["address"]['country'] : NULL;
      $addresses[] = implode(' ', array_filter($address));
    }
    // @todo allow multiple address lookup By now only first...
    return $addresses[0];

  }

}
