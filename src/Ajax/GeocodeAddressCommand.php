<?php

namespace Drupal\address_js_geocoder\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * The geocode address ajax command.
 */
class GeocodeAddressCommand implements CommandInterface {

  /**
   * The address widget it comes from.
   *
   * @var string
   */
  protected $fieldAddress;

  /**
   * The geofield target widget it will fill.
   *
   * @var string
   */
  protected $geoFieldTarget;


  /**
   * An address to pass to the method.
   *
   * @var array
   */
  protected $addressCoordinates;

  /**
   * Constructs an InvokeCommand object.
   *
   * @param array $field_address
   *   An array with the widget data selector and the field_name of
   *   the field address:
   *     - widget : string with the widget data selector
   *     - field_name: wtring with the field_name of current field address.
   * @param array $address_coordinates
   *   An array of address properties to pass to the method:
   *     - latitude
   *     - longitude.
   * @param array $geofield_target
   *   An array with the widget data selector and the field_name of
   *   the geofield_target:
   *     - widget : string with the widget data selector.
   *     - field_name: string the field_name of target geofield.
   */
  public function __construct(array $field_address, array $address_coordinates, array $geofield_target) {
    $this->addressCoordinates = $address_coordinates;
    $this->fieldAddress = $field_address;
    $this->geoFieldTarget = $geofield_target;
  }

  /**
   * Render custom ajax command.
   *
   * @return ajax
   *   Command function.
   */
  public function render() {
    return [
      'command' => 'geocode_address',
      'message' => 'My Awesome Message',
      'address_coordinates' => $this->addressCoordinates,
      'field_address' => $this->fieldAddress,
      'geofield_target' => $this->geoFieldTarget,
    ];
  }

}
