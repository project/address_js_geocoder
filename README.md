Drupal Module for display Address fields using ajax.

## INTRODUCTION

This will add a new widget for address fields to be able to geocode the address and
populate any geofield field, doing it with ajax.


## REQUIREMENTS

This module requires the following modules enabled in Drupal:

- [Geofield](https://www.drupal.org/project/geofield)
- [Geocode](https://www.drupal.org/project/geocode)
- [Address](https://www.drupal.org/project/address)

This module also requires a map provider.

## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit Drupal's documentation for further information.

## HOW IT WORKS

To get a correctly function it needs the `address field` and `geofield field`.

It takes the values of that fields `Country`,`Street Address`, `Postal Code` and `City` and formats 
the string to `${add1} ${add2}, ${locality}, ${postal_code} ${country}`to be seek by geocode module through ajax using the provider that's set in geocode configuration.

It can position the first resolved address automatically in geofield widget (if checkbox is checked) or do it manually when a select option is picked.

## CONFIGURATION

Once we have installed the module we must make a little configuration so that it works correctly.

- Configure the form display » Address Widget » Choose `Address geocoder` value » Set field target settings with your geofield map value(machine name).
- Edit Geofield » manage fields » Geocode » Geocode method » Click geocode from an existing field and choose your address field.
- Pick "single to multiple" for "Multi-value input handling".
- Finally select your Geocoder providers.

## MAINTAINERS

Maintained and supported by:

- Aleix - [Kinta](https://www.drupal.org/u/aleix)
- Communia - [Communia](https://www.drupal.org/communia).
