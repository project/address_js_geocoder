/**
* @file
*/

(function (Drupal) {
  Drupal.AjaxCommands.prototype.geocode_address = function (ajax, response, status) {
    // response.message: message from Command;
    // response.address_coordinates: coordinates as object with:
    //  - longitude
    //  - latitude
    // response.field_address: relevant info from address field:
    //  - widget: the data-drupal-selector of the wrapper of the field_address;
    //  - field_name: the field name of the address field.
    // response.geofield_target: Relevant info about target geofield:
    //  - widget: the data-drupal-selector of the wrapper of the target geofield
    //  - field_name: the field name of the target geofield.

    // If is needed the selected coordinates address string:
    // jQuery("[data-drupal-selector='" + response.field_address.widget + "'] select[name='" + response.field_address.field_name + "[0][geocoded_select]']");
    const lat_field = document.getElementsByName(response.geofield_target.field_name + "[0][value][lat]");
    const lon_field = document.getElementsByName(response.geofield_target.field_name + "[0][value][lon]")
    if( lat_field.length > 0) {
      lat_field[0].value = response.address_coordinates.latitude;
    }
    if( lon_field.length > 0) {
      const change_event = new Event('change');

      lon_field[0].value = response.address_coordinates.longitude;
      lon_field[0].dispatchEvent(change_event);
    }

}

Drupal.behaviors.address_js_geocoder = {
  attach: function (context, settings) {
    // must implode adrress to start geocode automatically
    var parsed_address = "";
    const field_address_context = context.querySelector(`[data-drupal-selector='${settings.address_selector}']`)
    if (field_address_context == null) return

    const address_elements = once('watch-address', 'input.address-line1, input.address-line2, input.postal-code, input.locality, select.country', field_address_context)

      for (const element of address_elements) {
      element.addEventListener('change', () => {
        var add1 = field_address_context.querySelector('input.address-line1').value
        var add2 = field_address_context.querySelector('input.address-line2').value + ","
        var postal_code = field_address_context.querySelector('input.postal-code').value
        var locality = field_address_context.querySelector('input.locality').value + ","
        var country = field_address_context.querySelector('select.country').value
        var autoupdate = field_address_context.querySelector('input[data-drupal-selector="' + settings.address_selector + '-autogeocode"]').checked
        parsed_address = `${add1} ${add2} ${locality} ${postal_code} ${country}`.replace(/  +/g, ' ');
        field_address_context.querySelector("[data-drupal-selector='" + settings.address_selector + "-container-resolved-address']").value = parsed_address
        if ( add1 && add1.length > 3 && locality && locality.length > 2 && country && country.length > 1 && autoupdate){
          // Create a synthetic click MouseEvent
          let mousedown = new MouseEvent("mousedown", {
            bubbles: true,
            cancelable: true,
            view: window,
          });
          field_address_context.querySelector("input[data-drupal-selector='" + settings.address_selector + "-request-geocode']").dispatchEvent(mousedown);
        }
      });
    }
  }
}
})(Drupal);
